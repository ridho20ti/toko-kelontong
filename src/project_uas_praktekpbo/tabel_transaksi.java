/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project_uas_praktekpbo;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.*;
import java.sql.*;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author LENOVO LEGION
 */
public class tabel_transaksi extends javax.swing.JFrame {

    /**
     * Creates new form tabel_transaksi
     */
    public tabel_transaksi() {
        initComponents();
        
        this.setLocationRelativeTo(null);
        tampil();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");

            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/toko_kelontong", "root", "");

        } catch (Exception e) {
            System.out.println("Koneksi gagal " + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        idTransaksi = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        hapus = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnCetak = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        inputBarang = new javax.swing.JButton();
        inputTransaksi = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        tabelBarang = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1800, 1050));
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 234, 217));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 28)); // NOI18N
        jLabel1.setText("TABEL DATA TRANSAKSI");

        idTransaksi.setForeground(new java.awt.Color(255, 234, 217));
        idTransaksi.setText("jLabel2");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(351, Short.MAX_VALUE)
                .addComponent(idTransaksi)
                .addGap(344, 344, 344)
                .addComponent(jLabel1)
                .addGap(691, 691, 691))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(idTransaksi)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1780, 130);

        jPanel4.setBackground(new java.awt.Color(242, 212, 186));
        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        hapus.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        hapus.setText("Hapus Data");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        jTable1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "null", "Title 7"
            }
        ));
        jTable1.setRowHeight(40);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(5).setMinWidth(50);
        }

        btnCetak.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCetak.setText("Cetak Data");
        btnCetak.setMaximumSize(new java.awt.Dimension(123, 31));
        btnCetak.setMinimumSize(new java.awt.Dimension(123, 31));
        btnCetak.setPreferredSize(new java.awt.Dimension(123, 31));
        btnCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 666, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, 666, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 740, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCetak, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel4);
        jPanel4.setBounds(340, 120, 1440, 870);

        jPanel2.setBackground(new java.awt.Color(255, 234, 217));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        inputBarang.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        inputBarang.setText("Input Data Barang");
        inputBarang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputBarangActionPerformed(evt);
            }
        });

        inputTransaksi.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        inputTransaksi.setText("Input Data Transaksi");
        inputTransaksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputTransaksiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(inputTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputBarang, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(61, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(113, Short.MAX_VALUE)
                .addComponent(inputTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addComponent(inputBarang, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(100, 100, 100))
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 120, 350, 440);

        jPanel3.setBackground(new java.awt.Color(255, 234, 217));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton3.setText("Tabel Transaksi");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        tabelBarang.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tabelBarang.setText("Tabel Barang");
        tabelBarang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tabelBarangActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                    .addComponent(tabelBarang, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(112, 112, 112)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addComponent(tabelBarang, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(97, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 550, 350, 440);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tampil() {
        String[] judul = {"NO", "ID TRANSAKSI", "TOTAL BELANJA", "JUMLAH BAYAR", "JUMLAH KEMBALIAN", "JUMLAH BELI", "TANGGAL TRANSAKSI"};
        DefaultTableModel model = new DefaultTableModel(null, judul);
        jTable1.setModel(model);
       
        String sql = "select * from transaksi";

        try {
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/toko_kelontong", "root", "");
            java.sql.Statement stm = koneksi.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            int i = 1;
            while (res.next()) {
                int no = i;
                String id = res.getString("id");
                String totalBelanja = res.getString("total_belanja");
                String jumlahBayar = res.getString("jumlah_bayar");
                String jumlahKembali = res.getString("jumlah_kembali");
                String jumlahBeli = res.getString("jumlah_beli");
                String tanggal = res.getString("tanggal_transaksi");
                             
                String[] data = {""+no ,id ,totalBelanja ,jumlahBayar, jumlahKembali, jumlahBeli, tanggal};
                model.addRow(data);
                i++;
            }//TAMBAHIN BOX UNTUK ID BIAR BISA DIHAPUS
            //TAMBAHIN SINTAK TEKAN TABEL

        } catch (Exception e) {
            System.out.println(e);
        }   
    }
    
    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
         try {
            String sql ="DELETE FROM transaksi WHERE id='"+idTransaksi.getText()+"'";
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/toko_kelontong", "root", "");
            java.sql.PreparedStatement pst = koneksi.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(this, "Data Transaksi Berhasil di Hapus");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    tampil();   
    }//GEN-LAST:event_hapusActionPerformed

    private void inputBarangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputBarangActionPerformed
        this.setVisible(false);
        new input_data_barang().setVisible(true);
    }//GEN-LAST:event_inputBarangActionPerformed

    private void inputTransaksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputTransaksiActionPerformed
        this.setVisible(false);
        new input_data_transaksi().setVisible(true);
    }//GEN-LAST:event_inputTransaksiActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void tabelBarangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tabelBarangActionPerformed
        this.setVisible(false);
        new tabel_barang().setVisible(true);
    }//GEN-LAST:event_tabelBarangActionPerformed

    private void btnCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakActionPerformed
       try{
            InputStream is = tabel_transaksi.class.getResourceAsStream("/project_uas_praktekpbo/transaksi.jasper");
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/toko_kelontong", "root", "");
            JasperPrint jsPrint = JasperFillManager.fillReport(is, null, koneksi);
            JasperViewer.viewReport(jsPrint, false);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "gagal mencetak laporan karena :"
            + e.getMessage(), "cetak laporan",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCetakActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int baris = jTable1.rowAtPoint(evt.getPoint());
        String getId = jTable1.getValueAt(baris, 1).toString();
        String sql = "SELECT * FROM transaksi where id='"+ getId +"'";

        try {
            Connection koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/toko_kelontong", "root", "");
            java.sql.Statement stm = koneksi.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()) {
                idTransaksi.setText(res.getString("id"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Database gagal "+e.getMessage());
        }
    }//GEN-LAST:event_jTable1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(tabel_transaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(tabel_transaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(tabel_transaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(tabel_transaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new tabel_transaksi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCetak;
    private javax.swing.JButton hapus;
    private javax.swing.JLabel idTransaksi;
    private javax.swing.JButton inputBarang;
    private javax.swing.JButton inputTransaksi;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton tabelBarang;
    // End of variables declaration//GEN-END:variables
}
